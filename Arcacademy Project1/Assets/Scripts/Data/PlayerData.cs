﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Data;

[System.Serializable]
public class PlayerData
{
    public int level, passedlevels, crystals, xp, maximumHp, maximumXp;
    public float defence, criticalHit, hp, power;
    public Attack[] availableattak;
}
