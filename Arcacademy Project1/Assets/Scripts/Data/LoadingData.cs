﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;

namespace Assets.Scripts.Data
{
    public class LoadingData
    {
        public PlayerData pData;
        public PlayerData Load(string gameDataFilename1, string gameDataFilename2)
        {
            string filePath1 = Path.Combine(Application.streamingAssetsPath, gameDataFilename1);
            string filePath2 = Path.Combine(Application.streamingAssetsPath, gameDataFilename2);

            if (File.Exists(filePath1) && File.Exists(filePath2))
            {

                string dataAsJson = File.ReadAllText(filePath1);
                Debug.Log(dataAsJson);
                PlayerData loadedData = JsonUtility.FromJson<PlayerData>(dataAsJson);
                pData = loadedData;
                dataAsJson = File.ReadAllText(filePath2);
                pData.availableattak = JsonHelper.FromJson<Attack>(dataAsJson);
                Debug.Log(dataAsJson);
            }
            else
            {
                Debug.LogError("Cannot load game data!");
            }
            return pData;
        }
       
    }
}
