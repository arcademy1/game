﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Data
{
    [System.Serializable]
    public class Attack
    {
        public int id, level, availableToLevel;
        public float power, criticalHit, cooldown; //power 0f : 1f for every skill besides Roar

    }
}
