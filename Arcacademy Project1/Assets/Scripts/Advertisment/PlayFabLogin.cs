using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public class PlayFabLogin : MonoBehaviour
{
	public ServerInfo server;
	
	public void Start()
	{
			
		//Note: Setting title Id here can be skipped if you have set the value in Editor Extensions already.
		if (string.IsNullOrEmpty(PlayFabSettings.TitleId)){
			PlayFabSettings.TitleId = ""; // Please change this value to your own titleId from PlayFab Game Manager
		}
		#if UNITY_ANDROID
		var request = new LoginWithAndroidDeviceIDRequest() { AndroidDeviceId = "GettingStartedGuide", CreateAccount = true};
		PlayFabClientAPI.LoginWithAndroidDeviceID(request, OnLoginSuccess, OnLoginFailure);
		#elif UNITY_IOS
		var request = new LoginWithIOSDeviceIDRequest() { DeviceId = "GettingStartedGuide", CreateAccount = true};
		PlayFabClientAPI.LoginWithIOSDeviceID(request, OnLoginSuccess, OnLoginFailure);
		#endif
	}

	private void OnLoginSuccess(LoginResult result)
	{
		Debug.Log("Congratulations, you made your first successful API call!");	
		server.getAdUrlFromServer(); //после успешного логина просим у сервера ссылки на контент
	}

	private void OnLoginFailure(PlayFabError error)
	{
		Debug.LogWarning("Something went wrong with your first API call.  :(");
		Debug.LogError("Here's some debug information:");
		Debug.LogError(error.GenerateErrorReport());
		
	}
}