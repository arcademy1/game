using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using PlayFab.Internal;
using PlayFab.Json;

public class ServerInfo : MonoBehaviour

{
	private static ServerInfo instance; //нужно, чтобы стартовать корутину из статичного метода
    private static string _graphicAdUrl, url; //ссылка на контент на сервере
	public static GameObject blankAd;
	
	// Use this for initialization
	void Start () {
	
		instance = this; //нужно, чтобы стартовать корутину из статичного метода
        blankAd = GameObject.Find("adImage"); //находим на сцене пустую картинку для рекламы
	}

	
	public void getAdUrlFromServer()  // api PlayFab: получаем ссылку на изображение
	{
		GetContentDownloadUrlRequest request = new GetContentDownloadUrlRequest ();
		request.Key = "localads/localAd_pic.jpg"; //адрес, по которому картинка рекламы лежит на сервере
		PlayFabClientAPI.GetContentDownloadUrl ( request, OnGetDownloadUrlSuccess, OnPlayFabCallbackError );
        RequestUrl();

    }

    static void OnGetDownloadUrlSuccess( GetContentDownloadUrlResult result)
	{
		_graphicAdUrl = result.URL;
		 Debug.Log("Graphic Ad URL: " + _graphicAdUrl);
		instance.StartCoroutine(instance.LoadAdIntoImage());
	}
   

    static void OnPlayFabCallbackError(PlayFabError error)
	{
		Debug.Log(error);
	}

	
	IEnumerator LoadAdIntoImage() //скачиваем и загружаем рекламу в пустую картинку
	{
		Texture2D tex;
		tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
		using (WWW www = new WWW(_graphicAdUrl))
		{
			yield return www;
			www.LoadImageIntoTexture(tex);
            
            blankAd.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f)); //после этого можно показать рекламу пользователю
		}
	}

    /*IEnumerator LoadJson() //скачиваем и загружаем рекламу в пустую картинку
    {
        
        WWW www = new WWW(_jsonUrl);
        yield return www;
        if (www.error == null)
        {
            //Processjson(www.data);
           Debug.Log(www.text);
        }
        else
        {
            Debug.Log("ERROR: " + www.error);
        }
    }*/
    private void RequestUrl()
    {
        PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
        {
            FunctionName = "requestURL", // Arbitrary function name (must exist in your uploaded cloud.js file)
            FunctionParameter = new { name = "YOUR NAME" }, // The parameter provided to your function
            GeneratePlayStreamEvent = true, // Optional - Shows this event in PlayStream
        }, OnCloudHelloWorld, OnErrorShared);
    }

    private static void OnCloudHelloWorld(ExecuteCloudScriptResult result)
    {
        // Cloud Script returns arbitrary results, so you have to evaluate them one step and one parameter at a time
        Debug.Log(JsonWrapper.SerializeObject(result.FunctionResult));
        JsonObject jsonResult = (JsonObject)result.FunctionResult;
        object messageValue;
        jsonResult.TryGetValue("messageValue", out messageValue); // note how “messageValue” directly corresponds to the JSON values set in Cloud Script
        url = (string)messageValue;
    }

    private static void OnErrorShared(PlayFabError error)
    {
        Debug.Log(error.GenerateErrorReport());
    }

    public void Click()
    {
        Application.OpenURL(url);
    }
}
