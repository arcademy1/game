﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using Assets.Scripts.GameEnv;

public class UnityAdsScript : MonoBehaviour {

    public int actionNum;
    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo") &  GameObject.Find("GameManager").GetComponent<GameManager>().allowDoubles)
        {

            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                switch (actionNum)
                {
                    case 0:
                        GameObject.Find("GameManager").GetComponent<Platformer2DUserControl>().Revive();
                        break;
                    case 1:
                        GameObject.Find("GameManager").GetComponent<GameManager>().DoubleXp(); 
                        break;
                    case 2:
                        GameObject.Find("GameManager").GetComponent<GameManager>().DoubleCrystal();
                        break;
                }
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
