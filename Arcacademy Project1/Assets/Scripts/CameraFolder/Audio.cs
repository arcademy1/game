﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        //Make sure the entry on the Prefs exist or proceed to create it
        if (!PlayerPrefs.HasKey("AudioToggle"))
        {
            AudioListener.volume = 1.0f;
            PlayerPrefs.SetInt("AudioToggle", 1);
        }
        else
        {
            AudioListener.volume = (float)PlayerPrefs.GetInt("AudioToggle");
        }
        PlayerPrefs.Save();
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
