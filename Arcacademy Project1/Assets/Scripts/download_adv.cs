﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class download_adv : MonoBehaviour {

    string url = "https://naotw-pd.s3.amazonaws.com/media-youtube/UHmP4nEOlEk.jpg";
    
    public GameObject Buttons;
    public GameObject Advert;
    Texture2D img;
    RawImage AdvertImage;

    public float waitTime;
    float timer;
    bool timerRunning = false;


    // Use this for initialization
    void Start ()
    {
        Advert.SetActive(false);
        AdvertImage = Advert.GetComponent<RawImage>();
        StartCoroutine(LoadImg());
    }

    IEnumerator LoadImg()
    {
        // Load advert image
        yield return 0;
        WWW imgLink = new WWW(url);
        yield return imgLink;
        AdvertImage.texture = imgLink.texture;

        Buttons.SetActive(false);
        timer = 0f;
        timerRunning = true;
        Advert.SetActive(true);
    }

	void Update()
    {
        if (timerRunning)
        {
            ShowAdvert();
        }
        
    }

    void ShowAdvert()
    {
        // Showing advert for waitTime seconds

        timer += Time.deltaTime;
        if (timer > waitTime)
        {
            timer = 0f;
            timerRunning = false;
            Advert.SetActive(false); // Hide avert
            Buttons.SetActive(true); // Unlock buttons
        }
    }

}
