﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Enemy
{
    class Swipe: MonoBehaviour
    {
        public GameObject player;
        private static float timeSwipe = 0.2f;
        private float timerSwipe;
        private System.Random rnd;

        CapsuleCollider2D triggerColl;

        void Awake()
        {
            triggerColl = GetComponent<CapsuleCollider2D>();
            rnd = new System.Random();
            
        }
        void OnEnable()
        {
            timerSwipe = timeSwipe;
        }
        void Update()
        {
            if (timerSwipe > 0)
            { timerSwipe -= Time.deltaTime; }
            else { this.gameObject.SetActive(false);}
        }
        void OnTriggerStay2D(Collider2D coll)
        {
            if (triggerColl.isActiveAndEnabled && timerSwipe <= 0)
            {
                float attackPower = player.GetComponent<PlatformerCharacter2D>().pData.power * (1 + player.GetComponent<PlatformerCharacter2D>().pData.availableattak[1].power);
                if(rnd.NextDouble() < player.GetComponent<PlatformerCharacter2D>().pData.availableattak[1].cooldown) { attackPower *= 1.5f; }
                if (coll.gameObject.tag == "Enemy")
                {
                    coll.gameObject.GetComponent<EnemyMovement>().GetThrust(3f);
                    coll.gameObject.GetComponent<EnemyMovement>().ReceiveDamage(attackPower);
                }
                if (coll.gameObject.tag == "BossEnemy")
                {
                    coll.gameObject.GetComponent<BossMovement>().GetThrust(3f);
                    coll.gameObject.GetComponent<BossMovement>().ReceiveDamage(attackPower);
                }
            }
        }

        
    }
}
