﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Assets.Scripts.Enemy
{
    class EnemyMovement : MonoBehaviour
    {
        Animator anim;
        Transform enemyTransform;
        Rigidbody2D enemyRB;
        SpriteRenderer sprite, backgroundLight;
        GameObject dMessage;

        public EnemyData eData; // enemy data
        public GameObject damageMessage;
        private bool hit = false; // is enemy hitting?
        public float speed = 1.0f; // enemy speed
        private bool freeze = false;
        private bool cooldown = false;
        private bool move = true; // is enemy moving? 
        private GameManager gm;
        private System.Random randomChoice;
        
        private void Awake()
        {
            randomChoice = new System.Random();
            gm = GameObject.Find("GameManager").gameObject.GetComponent<GameManager>();
            anim = GetComponent<Animator>();
            enemyTransform = GetComponent<Transform>();
            enemyRB = GetComponent<Rigidbody2D>();
            sprite = GetComponent<SpriteRenderer>();
            backgroundLight = this.gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();
            eData.power *= gm.levelNum*2/3;
            eData.hp *= gm.levelNum;
            eData.defence *= gm.levelNum;
            eData.bonusXp *= gm.levelNum;
            eData.criticalHit *= gm.levelNum;
            eData.bonusCrystals *= gm.levelNum;
        }

        private void Update()
        {
            anim.SetBool("hit", hit); // send hit flag to Animator
            CommonEnemyBehaviour();


            if (GameObject.Find("Player").GetComponent<PlatformerCharacter2D>().isDied == true) // if player is died
            { hit = false; move = false; }
        }

        private void CommonEnemyBehaviour()
        {
            if (move & !freeze) { enemyTransform.Translate(Vector3.left * speed * Time.deltaTime); } // moving
        }
       

        public void ReceiveDamage(float damagePoints)
        {
            
            eData.hp -= damagePoints - eData.defence;
            float randomDistanceX = 1 + Convert.ToSingle(randomChoice.Next(5, 8))/10;
            dMessage = Instantiate(damageMessage, enemyTransform.position + randomDistanceX * transform.up, Quaternion.identity);
            dMessage.transform.GetChild(0).GetComponent<Text>().text = "-" + Convert.ToString(Math.Round(damagePoints - eData.defence));
            Invoke("StopGettingDamage", 0.5f);
            StopCoroutine("Blink");
            StartCoroutine("Blink");
            if (eData.hp <= 0)
            { Die(); }
        }
        private void Die()
        {
            if (gm.levelNum <= 10) { FindObjectOfType<AudioManager>().Play("boarDie"); }
            else { FindObjectOfType<AudioManager>().Play("wolfDie"); }
            Destroy(this.gameObject);
            //GameObject.Find("Player").GetComponent<PlatformerCharacter2D>().GetHp(eData.bonusHp); // player gets bonusHp aftehr enemy death
            GameObject.Find("Player").GetComponent<PlatformerCharacter2D>().GetXp(eData.bonusXp);
            int j = 0;
            for (int i = 0; i < eData.bonusCrystals; i++)
            {
                if (i % 13 == 0)
                { j = 0; }
                if (i % 2 == 0)
                { gm.CrystalInitiate(enemyTransform.position.x + j * 0.2f, 0.7f); }
                else
                { gm.CrystalInitiate(enemyTransform.position.x + j * 0.2f); }
                ++j;
            }

        }

        void OnTriggerStay2D(Collider2D coll)
        {
            if (coll.gameObject.tag == "Player" ) // check if it's player
            {
                if (!freeze)
                {
                    move = false;
                    hit = true;
                    if (hit && !cooldown) // try to hit player
                    {
                        GameObject.Find("Player").GetComponent<PlatformerCharacter2D>().ReceiveDamage(eData.power, eData.level);
                        cooldown = true; Invoke("CooldownTimerEnd", eData.cooldown);
                    }

                }
                enemyRB.constraints = RigidbodyConstraints2D.FreezePositionX;
                enemyRB.constraints = RigidbodyConstraints2D.FreezeRotation;
            }
        }
        

        void OnTriggerExit2D(Collider2D coll)
        {
            if (coll.gameObject.tag == "Player")
            {
                if (!freeze)
                {
                    hit = false;
                    move = true;
                }
                enemyRB.constraints = RigidbodyConstraints2D.None;
                enemyRB.constraints = RigidbodyConstraints2D.FreezeRotation;
            }
        }
        public void GetThrust(float thrust = 0)
        {
            enemyRB.constraints = RigidbodyConstraints2D.None;
            enemyRB.constraints = RigidbodyConstraints2D.FreezeRotation;
            Invoke("AllowMove", 0.3f);
            //thrustTimer = 0.3f;
            hit = move = false;
            enemyRB.AddForce(transform.right * thrust, ForceMode2D.Impulse);
        }
        public void ListenRoar(float time)
        {
            Invoke("StopFreezing", time);
            hit = false;
            move = false;
            freeze = true;
            anim.speed = 0;
        }
        private void StopFreezing()
        {
            freeze = false;
            anim.speed = 1;
            move = true;
        }
        private void AllowMove()
        {
            move = true;
        }
        private void StopGettingDamage()
        {
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1);
            backgroundLight.color = new Color(backgroundLight.color.r, backgroundLight.color.g, backgroundLight.color.b, 1);
            StopCoroutine("Blink");
        }
        private void CooldownTimerEnd()
        {
            hit = false;
            move = true;
            cooldown = false;

        }

        IEnumerator Blink()
        { // red blinking
            while (true)
            {
                switch (sprite.color.a.ToString())
                {
                    case "1":
                        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0);
                        backgroundLight.color = new Color(backgroundLight.color.r, backgroundLight.color.g, backgroundLight.color.b, 0);
                        //Play sound
                        yield return new WaitForSeconds(0.1f);
                        break;
                    default:
                        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1);
                        backgroundLight.color = new Color(backgroundLight.color.r, backgroundLight.color.g, backgroundLight.color.b, 1);
                        //Play sound
                        yield return new WaitForSeconds(0.1f);
                        break;
                }
            }

        }
    }
}
