﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Enemy
{
    [System.Serializable]
    class EnemyData
    {
        public int   level, defence, bonusXp, bonusCrystals;
        public float power,cooldown, hp, criticalHit;
    }
}
