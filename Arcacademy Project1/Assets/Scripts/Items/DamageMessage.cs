﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageMessage : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        Invoke("DestroyMessage", 1.5f);
	}
	
	void DestroyMessage()
    {
        Destroy(this.gameObject);
    }
}
