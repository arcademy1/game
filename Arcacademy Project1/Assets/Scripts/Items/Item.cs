﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class Item : MonoBehaviour // crystal script
{
    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.tag == "Player")
        {
            GameObject.Find("Player").GetComponent<PlatformerCharacter2D>().GetCrystal();
            Destroy(this.gameObject);
        }
    }
}
