﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class menu : MonoBehaviour {

    GameObject dataFromPersistant;
    // Use this for initialization
    void Start() {
        dataFromPersistant = GameObject.Find("DataController");
        //Debug.Log(dataFromPersistant.GetComponent<GameDataController>().GetCurrentRoundData().score);
            }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ClickStart()
    {
        SceneManager.LoadScene(2);
    }

    public void ClickRate()
    {
        Application.OpenURL("http://arcademy.xyz/");
    }

    public void ClicMusic()
    {
        // Switch music status
        
        if (PlayerPrefs.GetInt("AudioToggle") == 1)
        {
            AudioListener.volume = 0.0f;
            PlayerPrefs.SetInt("AudioToggle", 0);
            
        }
        else
        {
            AudioListener.volume = 1.0f;
            PlayerPrefs.SetInt("AudioToggle", 1);
        }
        PlayerPrefs.Save();

    }
}
