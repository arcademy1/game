﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bars:MonoBehaviour
{
    public GameObject player;
    private PlatformerCharacter2D playerComp;
    private GameObject hp, xp;
    private Image imageHp, imageXp;
    private float playerHp, playerXp;
    private float maxHp;
    private float maxXp;

    void Awake()
    {
        hp = this.transform.GetChild(0).gameObject;
        xp = this.transform.GetChild(1).gameObject;
        imageHp = hp.GetComponent<Image>(); // hp get image to change lenght
        imageXp = xp.GetComponent<Image>(); // xp get image to change lenght
        playerComp = player.GetComponent<PlatformerCharacter2D>();
    }

    void Update()
    {
        if (playerComp.isDied == false) // if player is alive
        {
            playerHp = playerComp.pData.hp; // get player hp
            maxHp = playerComp.pData.maximumHp;
            double roundedHp = (Math.Round(playerHp) == 0) ? 1 : Math.Round(playerHp);
            hp.transform.GetChild(0).GetComponent<Text>().text = Convert.ToString(roundedHp) + "/" + Convert.ToString(maxHp);
            imageHp.fillAmount = playerHp/maxHp; // fill a part of hp line

            playerXp = playerComp.pData.xp;
            xp.transform.GetChild(0).GetComponent<Text>().text = Convert.ToString(playerComp.pData.level) + "LEVEL";
            maxXp = playerComp.pData.maximumXp;
            imageXp.fillAmount = playerXp/ maxXp;
        }
        else
        { imageHp.fillAmount = 0; } // make hp line invisible
    }
}

