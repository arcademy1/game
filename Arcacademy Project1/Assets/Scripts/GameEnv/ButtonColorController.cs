﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonColorController : MonoBehaviour {

    PlatformerCharacter2D player_data;
    Image img;
    public int availableForLevel;
    bool coolDown;
    float roarPoint, swipePoint, stormPoint, commonPoint;
	// Use this for initialization
	void Awake ()
    {
        player_data = GameObject.Find("Player").GetComponent<PlatformerCharacter2D>();
        img = this.GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (player_data.pData.level >= availableForLevel)
        {
            switch (this.name)
            {
                case "SuperFight1Button":
                    coolDown = player_data.roarCooldown;
                    break;
                case "SuperFight2Button":
                    coolDown = player_data.swipeCooldown;
                    break;
                case "SuperFight3Button":
                    coolDown = player_data.stormCooldown;
                    break;
            }
            if (coolDown & img.color != new Color(1f, 0, 0))
            {
                img.color = new Color(1f, 0, 0);
                if (img.fillAmount == 1)
                {
                    img.fillAmount = 0;
                    switch (this.name)
                    {
                        case "SuperFight1Button":
                            InvokeRepeating("FillButton", 0, player_data.pData.availableattak[0].cooldown / 500f);
                            break;
                        case "SuperFight2Button":
                            InvokeRepeating("FillButton", 0, player_data.pData.availableattak[1].cooldown / 500f);
                            break;
                        case "SuperFight3Button":
                            InvokeRepeating("FillButton", 0, player_data.pData.availableattak[2].cooldown / 500f);
                            break;
                    }
                }
                    
            }
            else if (!coolDown & img.color != new Color(1f, 1f, 1f)){ img.fillAmount = 1; img.color = new Color(1f, 1f, 1f); }
        }
        else { img.color = new Color(0.33f,0.23f, 1); }
	}
    
    void FillButton()
    {
        img.fillAmount += 0.002f;
        if (img.fillAmount == 1) { Debug.Log("Hello"); CancelInvoke(); }
    }
}
