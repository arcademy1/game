﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using Assets.Scripts.Data;


public class GameDataController 
{
    string[] skillsNames = { "RoarSkill", "SwipeSkill", "StormSkill", "IronSkin", "9Lives", "SharpClaw" };
    //дописать сохранение значений для определенного скила отдельно
    private PlayerData LoadPlayerData()
    {
        PlayerData data = new PlayerData();
        data.maximumXp = PlayerPrefs.GetInt("MaximumXp");
        data.maximumHp = PlayerPrefs.GetInt("MaximumHp");
        data.level = PlayerPrefs.GetInt("PlayerLevel");
        data.passedlevels = PlayerPrefs.GetInt("PassedLevels");
        data.crystals = PlayerPrefs.GetInt("CrystalsCount");
        data.xp = PlayerPrefs.GetInt("XP");
        data.power = PlayerPrefs.GetFloat("Power");
        data.defence = PlayerPrefs.GetFloat("Defence");
        data.criticalHit = PlayerPrefs.GetFloat("CriticalHit");
        data.hp = PlayerPrefs.GetFloat("HP");
        data.availableattak = LoadSkillsData();
        return data;
    }

    private Attack[] LoadSkillsData()
    {
        Attack[] skillData = new Attack[6];
        
        int i = 0;
        foreach (string name in skillsNames)
        {
            string[] s = PlayerPrefs.GetString(name).Split(',');
            skillData[i] = new Attack();
            skillData[i].id = i;
            skillData[i].level = Convert.ToInt32(s[1]);
            skillData[i].availableToLevel = Convert.ToInt32(s[2]);
            skillData[i].power = Convert.ToSingle(s[3]);
            skillData[i].criticalHit = Convert.ToSingle(s[4]);
            skillData[i].cooldown = Convert.ToSingle(s[5]);
            i++;
        }        
        return skillData;
    }

    public void SavePlayerData(PlayerData pData)
    {
        PlayerPrefs.SetInt("MaximumHp", pData.maximumHp);
        PlayerPrefs.SetInt("MaximumXp", pData.maximumXp);
        PlayerPrefs.SetInt("PlayerLevel", pData.level);
        PlayerPrefs.SetInt("PassedLevels", pData.passedlevels);
        PlayerPrefs.SetInt("CrystalsCount", pData.crystals);
        PlayerPrefs.SetInt("XP", pData.xp);
        PlayerPrefs.SetFloat("Power", pData.power);
        PlayerPrefs.SetFloat("Defence", pData.defence);
        PlayerPrefs.SetFloat("CriticalHit", pData.criticalHit);
        PlayerPrefs.SetFloat("HP", pData.hp);
        //SavePlayerSkillsData(false, pData.availableattak);
    }

    public void SavePlayerSkillsData(bool baseValues = true, Attack attackData = null)
    {
        if (!baseValues)
        {
            FieldInfo[] fields = attackData.GetType().GetFields();
            string formatData = "";
            foreach (FieldInfo field in fields)
            {
                formatData += Convert.ToString(field.GetValue(attackData));
                
                if (field != fields[fields.Length - 1])
                {formatData += ","; }
            }
            PlayerPrefs.SetString(skillsNames[Convert.ToInt32(attackData.id)], formatData);
        }
        else
        {
            PlayerPrefs.SetString("RoarSkill", "0,1,5,4,0,30");
            PlayerPrefs.SetString("SwipeSkill", "1,1,1,0.1,0.05,30");
            PlayerPrefs.SetString("StormSkill", "2,1,10,0.05,0,30");
            PlayerPrefs.SetString("IronSkin", "3,1,1,0.01,0,30");
            PlayerPrefs.SetString("9Lives", "4,1,5,0.01,0,30");
            PlayerPrefs.SetString("SharpClaw", "5,1,10,0.01,0,30");
        }
    }

    public PlayerData LoadData(bool update = false)
    {
        PlayerData pData = new PlayerData();
        if (!PlayerPrefs.HasKey("PassedLevels") || update)
        {
            pData.maximumXp = 100;
            pData.maximumHp = 100;
            pData.level = 1;
            pData.passedlevels = 0;
            pData.crystals = 0;
            pData.xp = 0;
            pData.power = 20;
            pData.defence =5;
            pData.criticalHit = 0.5f;
            pData.hp = 100f;
            SavePlayerData(pData);
            SavePlayerSkillsData();
            pData.availableattak = LoadSkillsData();
            Debug.Log(pData);
        }
        else
        {
            pData = LoadPlayerData();
        }
        return pData;
    }

    public Attack UpdateSkillLevel(Attack skill)
    {
        switch (skill.id)
        {
            case 0:
                skill.power += 0.1f;
                break;
            case 1:
                skill.power += 0.1f;
                skill.criticalHit += 0.05f;
                break;
            case 2:
                skill.power += 0.05f;
                break;
            default:
                skill.power +=0.1f;
                break;
        }

        skill.level += 1;
        skill.cooldown -= 0.5f;
        SavePlayerSkillsData(false, skill);
        return skill;
    }
}