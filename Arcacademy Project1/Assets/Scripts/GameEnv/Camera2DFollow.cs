using System;
using UnityEngine;

public class Camera2DFollow : MonoBehaviour
    {
        public Transform target;
        public float damping = 1;
        public bool globalStop = false;


        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;

        private bool stopCamera = false; // camera is stoped
        private float targetXStop; // camera stop position

        // Use this for initialization
        private void Start()
        {
            m_LastTargetPosition = target.position;
            m_OffsetZ = (transform.position - target.position).z;
            transform.parent = null;
            targetXStop = m_LastTargetPosition.x;
        }


        // Update is called once per frame
        private void Update()
        {
            if (target.GetComponent<PlatformerCharacter2D>().pData.hp >= 0 && !globalStop)
            {
                if (target.position.x - m_LastTargetPosition.x < 0 & !stopCamera) // if player go left stop camera
                {
                    stopCamera = true;
                    targetXStop = target.position.x;
                }
                else if (targetXStop <= target.position.x & stopCamera) // if player go right and pass targetXStop unblock camera
                {
                    stopCamera = false;
                }
                if (!stopCamera) // move camera with player
                {
                    Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ + new Vector3(3,1.7f,0);
                    Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

                    transform.position = newPos;

                    m_LastTargetPosition = target.position;

                }
            }
            
            
        }
}

