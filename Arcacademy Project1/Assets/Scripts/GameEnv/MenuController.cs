﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;
using Assets.Scripts.Data;


public class MenuController : MonoBehaviour
{
    public GameObject skillInfo, selectLvlPanel, startComic;
    public Sprite[] skillIcons, volumeIcons;
    public Sprite blackCircle, whiteCircle;
    public TextAsset textFile;
    public GameObject volumePauseButton;

    private PlayerData pData;
    private GameDataController dataController;
    private GameObject skillInfoDataContainer;
    private string[][] skillInfoList;
    private int currentSkillId, currentSkillPrice;
    private int locationNum;
    private bool showStartComic = false;

    private void Awake()
    {
        skillInfoDataContainer = skillInfo.transform.GetChild(2).gameObject;
        skillInfoList = new string[6][];
        LoadText();
        dataController = new GameDataController();
        
        if (!PlayerPrefs.HasKey("PlayerLevel"))
        { showStartComic = true; }
        if (!PlayerPrefs.HasKey("AudioToggle") )
        { AudioListener.volume = 1.0f; PlayerPrefs.SetInt("AudioToggle", 1); }
        UpdateVolumeSate();
        pData = dataController.LoadData();
        UpdatePlayerInformation();
        for (int i = 2; i < 8; ++i)
        { switch (i)
            {
                case 2:
                case 6:
                this.transform.GetChild(i).gameObject.GetComponent<Image>().color = (pData.level >= 5)? new Color(1,1,1) : new Color(0.5f,0.5f,0.5f);
                break;
                case 4:
                case 5:
                this.transform.GetChild(i).gameObject.GetComponent<Image>().color = (pData.level >= 10) ? new Color(1, 1, 1) : new Color(0.5f, 0.5f, 0.5f);
                break;
            }
            
        }
        //Debug.Log("START");
        //Debug.Log(skillInfoList[0][0]);
    }

    void UpdatePlayerInformation()
    { GameObject.Find("PlayerInformationData").GetComponent<Text>().text = CreatePlayerInformation(); }

    private string CreatePlayerInformation()
    {
        return Convert.ToString(pData.level) + "\n" + Convert.ToString(pData.xp)+"/"+ Convert.ToString(pData.maximumXp) + "\n" + Convert.ToString(pData.maximumHp)+ "\n" + Convert.ToString(pData.crystals) + "\n" + Convert.ToString(pData.power);
    }

    private void LoadText()
    {
        string[] linesInFile = textFile.text.Split('\n');
        int i = 0;
        foreach (string line in linesInFile)
        {
            skillInfoList[i] = line.Split(',');
            ++i;
        }
    }

    private void Update()
    {
        //show start comic
        if (showStartComic)
        { FindObjectOfType<AudioManager>().Play("firstComic"); startComic.SetActive(true); showStartComic = false; Invoke("StopShowStartComic", 9f); }

        // skills buttons and start
        if (CrossPlatformInputManager.GetButtonUp("Increase"))
        {
            
            if (pData.crystals >= currentSkillPrice * pData.availableattak[currentSkillId].level)
            {   FindObjectOfType<AudioManager>().Play("click");
                pData.crystals -= currentSkillPrice * pData.availableattak[currentSkillId].level;
                pData.availableattak[currentSkillId] = dataController.UpdateSkillLevel(pData.availableattak[currentSkillId]);
                
                dataController.SavePlayerData(pData);
                UpdatePlayerInformation();
                ChooseSkill(currentSkillId);
            }
        }
        if (CrossPlatformInputManager.GetButtonUp("RoarSkill"))
        {
            
            if (pData.level >= 5)
            { FindObjectOfType<AudioManager>().Play("click");ChooseSkill(0); }
        }
        if (CrossPlatformInputManager.GetButtonUp("SwipeSkill"))
        {
            FindObjectOfType<AudioManager>().Play("click");
            ChooseSkill(1);
        }
        if (CrossPlatformInputManager.GetButtonUp("StormSkill"))
        {
           
            if ( pData.level >= 10)
            { FindObjectOfType<AudioManager>().Play("click");ChooseSkill(2); }
        }
        if (CrossPlatformInputManager.GetButtonUp("IronSkin"))
        {
            FindObjectOfType<AudioManager>().Play("click");
            ChooseSkill(3);
        }
        if (CrossPlatformInputManager.GetButtonUp("9Lives"))
        {
            
            if (pData.level >=5)
            { FindObjectOfType<AudioManager>().Play("click");ChooseSkill(4); }
            
        }
        if (CrossPlatformInputManager.GetButtonUp("SharpClaw"))
        {
            
            if (pData.level >= 10)
            { FindObjectOfType<AudioManager>().Play("click");ChooseSkill(5); }
        }
        if (CrossPlatformInputManager.GetButtonUp("CloseSkillInfo"))
        {
            Destroy(GameObject.FindWithTag("SkillPanel"));
        }
        if (CrossPlatformInputManager.GetButtonUp("Volume"))
        {
            FindObjectOfType<AudioManager>().Play("click");
            if (PlayerPrefs.GetInt("AudioToggle") == 1)
            {
                AudioListener.volume = 0.0f;
                PlayerPrefs.SetInt("AudioToggle", 0);
                volumePauseButton.GetComponent<Image>().sprite = volumeIcons[1];
            }
            else
            {
                AudioListener.volume = 1.0f;
                PlayerPrefs.SetInt("AudioToggle", 1);
                volumePauseButton.GetComponent<Image>().sprite = volumeIcons[0];
            }
            PlayerPrefs.Save();
        }
        if (CrossPlatformInputManager.GetButtonUp("Start"))
        {
            FindObjectOfType<AudioManager>().Play("click");
            if (!PlayerPrefs.HasKey("PlayFabAd"))
            {
                GameObject.Find("adImage").GetComponent<Animator>().SetBool("start", true);
                Time.timeScale = 1;
                PlayerPrefs.SetInt("PlayFabAd", 1);
                Invoke("ShowLocations", 1f);
            }
            else { ShowLocations(); }
        }

        //Locations
        if (CrossPlatformInputManager.GetButtonUp("ForestButton"))
        {
            FindObjectOfType<AudioManager>().Play("click");
            locationNum = 1;
            LoadLvlPanel();
        }
        if (CrossPlatformInputManager.GetButtonUp("SteppeButton"))
        {
            FindObjectOfType<AudioManager>().Play("click");
            locationNum = 2;
            LoadLvlPanel();
        }
        if (CrossPlatformInputManager.GetButtonUp("BackToMenu"))
        {
            FindObjectOfType<AudioManager>().Play("click");
            selectLvlPanel.transform.GetChild(1).gameObject.SetActive(true);
            selectLvlPanel.transform.GetChild(2).gameObject.SetActive(false);
            selectLvlPanel.SetActive(false);
        }

        //lvls
        TrackLvlClick();

    }
    void LoadLvlPanel()
    {
        selectLvlPanel.transform.GetChild(1).gameObject.SetActive(false);
        selectLvlPanel.transform.GetChild(2).gameObject.SetActive(true);
        for (int lvlNum = 1; lvlNum < 10; ++lvlNum)
        {
            selectLvlPanel.transform.GetChild(2).transform.GetChild(lvlNum).transform.GetChild(0).GetComponent<Text>().text = "LVL" + Convert.ToString(lvlNum);
            if (lvlNum + locationNum * 10 - 10 > pData.passedlevels + 1)
            { selectLvlPanel.transform.GetChild(2).transform.GetChild(lvlNum).GetComponent<Image>().color = new Color(0, 0, 0, 0.5f); }
            else
            {
                selectLvlPanel.transform.GetChild(2).transform.GetChild(lvlNum).GetComponent<Image>().color = new Color(0, 0, 0, 1);
            }
        }
        Debug.Log(pData.passedlevels + 1);
        GameObject.Find("BossLvl").GetComponent<Image>().color = (locationNum * 10 > pData.passedlevels + 1)?new Color(0, 0, 0, 0.5f) : new Color(0, 0, 0, 1);
    }

    void TrackLvlClick()
    {
        if (CrossPlatformInputManager.GetButtonUp("Lvl1"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 1 : 11; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("Lvl2"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 2 : 12; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("Lvl3"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 3 : 13; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("Lvl4"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 4 : 14; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("Lvl5"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 5 : 15; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("Lvl6"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 6 : 16; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("Lvl7"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 7 : 17; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("Lvl8"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 8 : 18; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("Lvl9"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 9 : 19; StartGame(); }
        if (CrossPlatformInputManager.GetButtonUp("BossLvl"))
        { LevelNumData.currentLevel = (locationNum == 1) ? 10 : 20; StartGame(); }
    }

    void StartGame()
    {

        if (LevelNumData.currentLevel <= pData.passedlevels + 1)
        {
            FindObjectOfType<AudioManager>().Play("click");
            FindObjectOfType<SDKInit>().ShowInterstitial();
            Time.timeScale = 0;
            SceneManager.LoadScene("Game");
        }
    }

    void ChooseSkill(int skillId)
    {

        Destroy(GameObject.FindWithTag("SkillPanel"));
        int numChildren = skillInfoDataContainer.transform.childCount;
        string[] skillData = skillInfoList[skillId];
        Attack currentAttack = dataController.LoadData().availableattak[skillId];
        currentSkillId = currentAttack.id;
        currentSkillPrice = Convert.ToInt32(skillData[4]);

        for (int i = 0; i < numChildren; ++i)
        {
            GameObject child = skillInfoDataContainer.transform.GetChild(i).gameObject;

            switch (i)
            {
                case 0:
                    child.GetComponent<Image>().sprite = skillIcons[Convert.ToInt32(skillData[0])];
                    break;
                case 4:
                    child.GetComponent<Text>().text = "Level " + Convert.ToString(currentAttack.level);
                    break;
                case 5:
                    for (int levelNum = 0; levelNum < 18; ++levelNum)
                    {
                        if(levelNum < currentAttack.level)
                        { child.transform.GetChild(levelNum).gameObject.GetComponent<Image>().sprite = blackCircle;}
                        else
                        { child.transform.GetChild(levelNum).gameObject.GetComponent<Image>().sprite = whiteCircle;}
                        
                    }
                    break;
                case 6:
                    if (currentAttack.level < 18)
                    {
                        child.SetActive(true);
                        child.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Increase for " + Convert.ToString(currentSkillPrice * currentAttack.level) + " crystals";
                        child.GetComponent<Image>().color =(currentSkillPrice * currentAttack.level>pData.crystals)? new Color(0.5f, 0.5f, 0.5f): new Color(0, 0, 0); 
                    }
                    else
                    { child.SetActive(false); }
                    break;
                default:
                    child.gameObject.GetComponent<Text>().text = skillData[i];
                    break;

            }
        }
        Instantiate(skillInfo);
    }

    void UpdateVolumeSate()
    {
        if (PlayerPrefs.GetInt("AudioToggle") == 0)
        { AudioListener.volume = 0.0f; volumePauseButton.GetComponent<Image>().sprite = volumeIcons[1];}
        else
        { AudioListener.volume = 1f; volumePauseButton.GetComponent<Image>().sprite = volumeIcons[0];  }
    }

    void StopShowStartComic()
    { FindObjectOfType<AudioManager>().FadeStop("firstComic"); startComic.SetActive(false); }

    void ShowLocations()
    {
        GameObject.Find("adImage").GetComponent<Animator>().SetBool("start", false);
        
        selectLvlPanel.SetActive(true);
    }

}


