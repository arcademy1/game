﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.GameEnv;
using UnityStandardAssets.CrossPlatformInput;


public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject player, crystalCount, crystal, platform, boss1, boss2, endComic;
    public GameObject[] location1Enemies;
    public GameObject[] location2Enemies;
    public GameObject[] location1Env;
    public GameObject[] location2Env;
    public GameObject wall; // because of this object player can't go left

    private GameObject boss;
    private GameObject[] commonEnemies;
    private GameDataController dataController; // to load and save data
    private Rigidbody2D playerRB;
    private GameObject enemy;
    private float startX, levelDistance;
    private float pastSpawn;
    public bool isEnd; // end of level
    public bool allowDoubles = true;

    private float distanceBtwnWallAndPlayer;

    public int levelNum;
    public int startXp, startCrystalCount, startLvl;
    private int EnemyCountToEnd;
    public int enemyCountOnLevel;
    public bool success;
    public int earnedXp, earnedCrystals;
    public string soundtrackName;

    private void Awake()
    {
        dataController = new GameDataController();
        playerRB = player.GetComponent<Rigidbody2D>();
        distanceBtwnWallAndPlayer = player.transform.position.x - wall.transform.position.x;
        //player.GetComponent<PlatformerCharacter2D>().pData = dataController.LoadData(true); // refresh pplayer data (ONLY FOR TEST)
        Time.timeScale = 1;
        Restart(-1);
        
    }
    public void Restart(int choosenLevel = -1)
    {
        
        allowDoubles = true;
        if (choosenLevel == -1) { levelNum = LevelNumData.currentLevel; }
        else if (choosenLevel == -2)
        { levelNum = LevelNumData.currentLevel + 1; LevelNumData.currentLevel = levelNum;}
        else
        { levelNum = choosenLevel;  }

        player.GetComponent<PlatformerCharacter2D>().pData = dataController.LoadData();
        player.GetComponent<PlatformerCharacter2D>().pData.hp = player.GetComponent<PlatformerCharacter2D>().pData.maximumHp;
        player.GetComponent<PlatformerCharacter2D>().stormCooldown = false;
        player.GetComponent<PlatformerCharacter2D>().swipeCooldown = false;
        player.GetComponent<PlatformerCharacter2D>().roarCooldown = false;
        player.GetComponent<PlatformerCharacter2D>().stopInvoke();
        startX = player.transform.position.x; //start x coord
        pastSpawn = startX;
        
        if (levelNum<11)
        {
            EnemyCountToEnd = levelNum * 5;
            soundtrackName = "level1Soundtrack";
            this.GetComponent<Parallax>().changeLocation(1);
            commonEnemies = location1Enemies;
            boss = boss1;
            player.transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(1,0.17f,0);
            foreach (GameObject env1 in location1Env)
            {
                env1.SetActive(true);
            }
            foreach (GameObject env2 in location2Env)
            {
                env2.SetActive(false);
            }
            
        }
        else
        {
            EnemyCountToEnd = levelNum + 40;
            soundtrackName = "level2Soundtrack";
            this.GetComponent<Parallax>().changeLocation(2);
            player.transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(0, 0.83f,1);
            commonEnemies = location2Enemies;
            boss = boss2;
            foreach (GameObject env1 in location1Env)
            {
                env1.SetActive(false);
            }
            foreach (GameObject env2 in location2Env)
            {
                env2.SetActive(true);
            }
        }
        
        enemyCountOnLevel = 0; 
        isEnd = false;
        if (!success)
        { player.transform.position = new Vector3(player.transform.position.x, -2.2f, player.transform.position.z); }
        success = false;
        DestroyAllObjects("Enemy");
        DestroyAllObjects("Crystal");
        startXp = player.GetComponent<PlatformerCharacter2D>().pData.xp;
        earnedXp = 0;
        earnedCrystals = 0;
        startCrystalCount = player.GetComponent<PlatformerCharacter2D>().pData.crystals;
        startLvl = player.GetComponent<PlatformerCharacter2D>().pData.level;
        if(levelNum % 10 == 0)
        { soundtrackName = "bossSoundtrack"; GenerateBoss(); }
        Debug.Log("Level:"); Debug.Log(levelNum);
        FindObjectOfType<AudioManager>().Play(soundtrackName);
        FindObjectOfType<AudioManager>().Play("appear");
        // player.GetComponent<PlatformerCharacter2D>().GetXp(100000);

    }
    private void DestroyAllObjects(string tag)
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(tag);

        for (var i = 0; i < gameObjects.Length; i++)
        {
            Destroy(gameObjects[i]);
        }
    }

    public void CrystalInitiate(float x, float y = 0.1f)
    {
        Instantiate(crystal, new Vector3(x, y, 1), Quaternion.identity);
    }

    private void Update()
    {
        if (levelNum % 10 != 0)
        {
            
            levelDistance = player.transform.position.x;// - startX;

            if (!isEnd & enemyCountOnLevel >= EnemyCountToEnd & GameObject.FindWithTag("Enemy") == null & GameObject.FindWithTag("Crystal") == null)
            {
                SuccessFinish();
                //GenerateBoss();
            }

            if (!isEnd)
            {
                if (enemyCountOnLevel < EnemyCountToEnd)
                {
                    SpawnEnemy();
                }
                MovegWall();
                if (platform.transform.position.x - player.transform.position.x < 7)
                {
                    platform.transform.position += new Vector3(3, 0, 0);
                }
            }

            if (GameObject.FindWithTag("Crystal") == null) // if there are no crystals initiate one
            {
                CrystalInitiate(player.transform.position.x + 12);
                CrystalInitiate(player.transform.position.x + 18);
                CrystalInitiate(player.transform.position.x + 25);
            }
        }
        else
        {
            if(!isEnd & GameObject.FindWithTag("BossEnemy") == null & GameObject.FindWithTag("Crystal") == null)
            {
                SuccessFinish();
            }
        }
        if (player.GetComponent<PlatformerCharacter2D>().isDied)
        {
            FindObjectOfType<AudioManager>().FadeStop(soundtrackName);
            
            isEnd = true;
            success = false;
        }
        crystalCount.GetComponent<Text>().text = player.GetComponent<PlatformerCharacter2D>().pData.crystals.ToString(); // update crystal count

        
    }
    private void SuccessFinish()
    {
        if (levelNum != 20) { FindObjectOfType<AudioManager>().FadeStop(soundtrackName); }
        isEnd = true;
        success = true;
        

        if (player.GetComponent<PlatformerCharacter2D>().pData.passedlevels < levelNum)
        { player.GetComponent<PlatformerCharacter2D>().pData.passedlevels = levelNum; }
        dataController.SavePlayerData(player.GetComponent<PlatformerCharacter2D>().pData);//saving data to playerprefs        
        /*if (player.GetComponent<PlatformerCharacter2D>().pData.level == startLvl)
        { earnedXp = player.GetComponent<PlatformerCharacter2D>().pData.xp - startXp; }
        else
        { earnedXp += player.GetComponent<PlatformerCharacter2D>().pData.xp - startXp; }*/
        earnedCrystals = player.GetComponent<PlatformerCharacter2D>().pData.crystals - startCrystalCount;
        if (levelNum == 20)
        {
            FindObjectOfType<AudioManager>().Stop(soundtrackName);
            FindObjectOfType<AudioManager>().Play("lastComic");
            endComic.SetActive(true);
            Time.timeScale = 1;
            Invoke("MusicFading", 14f);
            Invoke("EndGame",16f);
        }
    }
    private void MusicFading()
    {
        FindObjectOfType<AudioManager>().FadeStop("lastComic");

    }
    private void EndGame()
    {
        Debug.Log("END");
        //FindObjectOfType<AudioManager>().Stop(soundtrackName);
        
        Time.timeScale = 0;
        SceneManager.LoadScene("Profile");
    }
    private void SpawnEnemy()
    {
        int enemyCount;
        if (pastSpawn - levelDistance < 5f) // enemies spawn
        {
            pastSpawn = levelDistance + 12;
            enemyCount = UnityEngine.Random.Range(1, 4); // how much enemies ? 1:3
            enemyCountOnLevel += enemyCount;

            for (int i = 1; i < enemyCount+1; i++)
            {
                enemy = Instantiate(commonEnemies[UnityEngine.Random.Range(0, 3)], new Vector3(pastSpawn + i, -2.05f, 1), Quaternion.identity); // what type of enemy
                enemy.SetActive(true);
            }
        }
    }
    private void MovegWall()
    {
        if (playerRB.velocity.x>0 && player.transform.position.x - wall.transform.position.x >= distanceBtwnWallAndPlayer)
        { wall.transform.position = new Vector3(player.transform.position.x - distanceBtwnWallAndPlayer, 0, 0); } //move wall with player
    }
    private void GenerateBoss()
    {
        Camera cam = Camera.main;
        cam.GetComponent<Camera2DFollow>().globalStop = true;
        Instantiate(wall, new Vector3(player.transform.position.x + 12, 0,0), Quaternion.identity);
        Instantiate(boss, new Vector3(player.transform.position.x + 9, -2.17f, 1), Quaternion.identity);
    }
    public void DoubleXp()
    {

            player.GetComponent<PlatformerCharacter2D>().GetXp(earnedXp);
            dataController.SavePlayerData(player.GetComponent<PlatformerCharacter2D>().pData);//saving data to playerprefs
            string doubleXp = Convert.ToString(earnedXp);
            GameObject.Find("PassedLevelInformation").GetComponent<Text>().text = "You received:\n" + doubleXp + " xp!\n" + Convert.ToString(earnedCrystals) + " crystals!";
            GameObject.Find("DoubleXp").GetComponent<Image>().color = new Color(0.6f, 0.6f, 0.6f);
            GameObject.Find("DoubleCrystals").GetComponent<Image>().color = new Color(0.6f, 0.6f, 0.6f);
            allowDoubles = false;

        
    }
    public void DoubleCrystal()
    {

            player.GetComponent<PlatformerCharacter2D>().pData.crystals += earnedCrystals;
            dataController.SavePlayerData(player.GetComponent<PlatformerCharacter2D>().pData);//saving data to playerprefs
            string doubleCrystals = Convert.ToString(earnedCrystals * 2);
            GameObject.Find("PassedLevelInformation").GetComponent<Text>().text = "You received:\n" + Convert.ToString(earnedXp) + " xp!\n" + doubleCrystals + " crystals!";
            GameObject.Find("DoubleCrystals").GetComponent<Image>().color = new Color(0.6f,0.6f,0.6f);
            GameObject.Find("DoubleXp").GetComponent<Image>().color = new Color(0.6f, 0.6f, 0.6f);
            allowDoubles = false;

    }

}


