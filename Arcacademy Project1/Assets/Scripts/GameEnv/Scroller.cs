﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameEnv
{
    class Scroller : MonoBehaviour
    {
        SpriteRenderer spriteR;
        //public GameObject[] sprites;
        List<GameObject> children = new List<GameObject>();
        private float heightCamera;
        private float widthCamera;

        private Vector3 PositionCam;
        private Camera cam;

        private void Awake()
        {
            cam = Camera.main;
            heightCamera = 2f * cam.orthographicSize;
            widthCamera = heightCamera * cam.aspect;
            
            foreach (Transform child in transform)
            {
                children.Add(child.gameObject);
            }
        }
        void Update()
        {
            foreach (var item in children)
            {
                spriteR = item.GetComponent<SpriteRenderer>();
                if ((spriteR.transform.position.x + spriteR.bounds.size.x / 2) < (cam.transform.position.x + widthCamera / 2))
                {
                    item.transform.position = new Vector3((spriteR.transform.position.x + (spriteR.bounds.size.x / 2)), spriteR.transform.position.y, spriteR.transform.position.z);
                }

            }
        }
    }
}
