using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;


public class Platformer2DUserControl : MonoBehaviour
{
    public GameObject player, hud, pausemenu, gameCamera, volumePauseButton, completeLevelScreen, failLevelScreen;
    public Sprite filledStar, emptyStar;
    public Sprite[] pauseVolumeImages;
    private GameManager gm;
    private PlatformerCharacter2D m_Character;
    private AudioListener audioListener;
    private bool fight = false;


    private void Awake()
    {
        m_Character = player.GetComponent<PlatformerCharacter2D>();
        audioListener = gameCamera.GetComponent<AudioListener>();
        gm = this.GetComponent<GameManager>();
        //pauseVolumeImage = volumePauseButton.GetComponent<Image>();
    }

    private void Update()
    {
        if(gm.isEnd)
        {
            if (gm.levelNum != 20)
                Time.timeScale = 0; 

            if (gm.success & !completeLevelScreen.activeSelf)
            {
                Debug.Log("complete"); hud.SetActive(false); completeLevelScreen.SetActive(true);
                if (gm.levelNum == 10)
                {
                    GameObject.Find("NextLevelButton").GetComponent<Image>().color = new Color(1, 1, 1, 0);
                }
                else if (GameObject.Find("NextLevelButton").GetComponent<Image>().color == new Color(1, 1, 1, 0))
                { GameObject.Find("NextLevelButton").GetComponent<Image>().color = new Color(1, 1, 1, 1); }

                //Information
                GameObject.Find("DoubleXp").GetComponent<Image>().color = new Color(1, 1, 1);
                GameObject.Find("DoubleCrystals").GetComponent<Image>().color = new Color(1, 1, 1);
                GameObject.Find("PassedLevelInformation").GetComponent<Text>().text = "You received:\n" + gm.earnedXp + " xp!\n" + gm.earnedCrystals + " crystals!";
                //Rate
                float percentHp = m_Character.pData.hp / m_Character.pData.maximumHp;
                Debug.Log(percentHp);

                if ( percentHp> 0.3f)
                { GameObject.Find("RateStars").transform.GetChild(0).GetComponent<Image>().sprite = filledStar; }
                else { GameObject.Find("RateStars").transform.GetChild(0).GetComponent<Image>().sprite = emptyStar; }
                if (percentHp > 0.5f)
                { GameObject.Find("RateStars").transform.GetChild(1).GetComponent<Image>().sprite = filledStar; }
                else { GameObject.Find("RateStars").transform.GetChild(1).GetComponent<Image>().sprite = emptyStar; }
                if (percentHp > 0.7f)
                { GameObject.Find("RateStars").transform.GetChild(2).GetComponent<Image>().sprite = filledStar; }
                else { GameObject.Find("RateStars").transform.GetChild(2).GetComponent<Image>().sprite = emptyStar; }
            }
            else if (!gm.success & !failLevelScreen.activeSelf )
            { Debug.Log("fail"); hud.SetActive(false);failLevelScreen.SetActive(true); }
            
        }
        ClickTracking();

    }

    private void ClickTracking()
    {
        // complete or fail level panel
        
        if (CrossPlatformInputManager.GetButtonUp("Menu"))
        {
            FindObjectOfType<AudioManager>().Stop(gm.soundtrackName);
            SceneManager.LoadScene("Profile");
        }

        if (CrossPlatformInputManager.GetButtonUp("RetryLevel"))
        {
            hud.SetActive(true); Time.timeScale = 1; completeLevelScreen.SetActive(false); failLevelScreen.SetActive(false);
            player.GetComponent<PlatformerCharacter2D>().isDied = false;
            gm.Restart(LevelNumData.currentLevel);
        }

        if (CrossPlatformInputManager.GetButtonUp("NextLevel"))
        {
            if (gm.levelNum != 10)
            {
                hud.SetActive(true); Time.timeScale = 1; completeLevelScreen.SetActive(false); //failLevelScreen.SetActive(false);
                player.GetComponent<PlatformerCharacter2D>().isDied = false;
                gm.Restart(-2);
                FindObjectOfType<AudioManager>().Play(gm.soundtrackName);

            }
        }

        if (CrossPlatformInputManager.GetButtonUp("Revive"))
        {
            
        }

            // pause menu
            if (CrossPlatformInputManager.GetButtonUp("Volume"))
        {
            if (PlayerPrefs.GetInt("AudioToggle") == 1)
            {
                AudioListener.volume = 0.0f;
                PlayerPrefs.SetInt("AudioToggle", 0);
                volumePauseButton.GetComponent<Image>().sprite = pauseVolumeImages[1];
            }
            else
            {
                AudioListener.volume = 1.0f;
                PlayerPrefs.SetInt("AudioToggle", 1);
                volumePauseButton.GetComponent<Image>().sprite = pauseVolumeImages[0];
            }
            PlayerPrefs.Save();
        }
        if (CrossPlatformInputManager.GetButtonUp("Resume"))
        { hud.SetActive(true); Time.timeScale = 1; pausemenu.SetActive(false); }

        //game interface buttons
        if (CrossPlatformInputManager.GetButtonUp("Pause"))
        {
            Debug.Log("pause");
            hud.SetActive(false);
            Time.timeScale = 0;
            pausemenu.SetActive(true);
            if (PlayerPrefs.GetInt("AudioToggle") == 1)
            { volumePauseButton.GetComponent<Image>().sprite = pauseVolumeImages[0]; }
            else
            { volumePauseButton.GetComponent<Image>().sprite = pauseVolumeImages[1]; }
        }
        if (CrossPlatformInputManager.GetButtonDown("Fight")) { fight = true; }
        if (CrossPlatformInputManager.GetButtonUp("Fight")) { fight = false; }

        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        bool roar = (!m_Character.roarCooldown & m_Character.pData.level >= 5) ? CrossPlatformInputManager.GetButtonUp("Roar") : false; 
        bool swipe = (!m_Character.swipeCooldown) ? CrossPlatformInputManager.GetButtonUp("Swipe") : false;
        bool storm = (!m_Character.stormCooldown & m_Character.pData.level >= 10) ? CrossPlatformInputManager.GetButtonUp("Storm") : false;
        if (player != null)
        { m_Character.Move(h, fight, roar, swipe, storm); }
    }

    public void Revive()
    {
        player.GetComponent<PlatformerCharacter2D>().Alive();
        hud.SetActive(true);
        failLevelScreen.SetActive(false);
        gm.isEnd = false;
        if (gm.levelNum % 10 != 0)
        {
            Debug.Log(gm.levelNum % 10);
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Enemy");
            for (var i = 0; i < gameObjects.Length; i++)
            {
                Destroy(gameObjects[i]);
            }
        }
        Time.timeScale = 1;
    }

}

