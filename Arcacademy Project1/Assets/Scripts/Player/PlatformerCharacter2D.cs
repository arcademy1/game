using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Assets.Scripts.Data;
using Assets.Scripts.Enemy;

public class PlatformerCharacter2D : MonoBehaviour
{
    [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
    [SerializeField] private float m_MinSpeed = 5f;

          
    private Rigidbody2D m_Rigidbody2D;
    public PlayerData pData; // player data
    public GameObject swipeObj; // needs for swipe collider
    public GameObject damageMessage;
    private CapsuleCollider2D swipeColl;
    private CircleCollider2D triggerColl;
    private SpriteRenderer sprite;
    public GameObject wall; // because of this object player can't go left
    private GameManager gm;

    private float previousTriggerCollRadius;
    private bool isFight = false;
    private bool rightMove = false; // player go right
    private float distanse; // between player and wall
    private Animator anim; // Reference to the player's animator component.
    private System.Random _rnd = new System.Random(); // for events dependent on chance

    public bool isDied = false; // if player is died
    bool runForward = false;
    bool runBackward = false;
    bool getDamage = false;
    bool fight = false;
    bool roar = false; // super hit Roar active
    bool swipe = false; // super hit Swipe active
    bool storm = false; // super hit Storm active
    bool thrustAllow = false;
    public bool roarCooldown = false;
    public bool swipeCooldown = false;
    public bool stormCooldown = false;
    bool ironSkinCooldown = false;
    bool nineLiveCooldown = false;
    bool sharpClawCooldown = false;
    int fightId = 0;
    int xpToNextLevel;

    float timerCooldown;
    //float timerBlinking; 
    float timerDamage = 1.5f; // get damage timer
    float timerStopMoving = 0;
    float timerSwipe = 0;
    GameObject dMessage;


    private void Awake()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        anim = GetComponent<Animator>();
        triggerColl = GetComponent<CircleCollider2D>();
        sprite = GetComponent<SpriteRenderer>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        swipeColl = swipeObj.GetComponent<CapsuleCollider2D>();
        damageMessage.transform.GetChild(0).GetComponent<Text>().color = new Color(0.75f,1,0.6f);
        previousTriggerCollRadius = triggerColl.radius;
        //pData = new LoadingData().Load("playerdata.json", "attackdata.json"); // load player data
        //distanse = this.transform.position.x - wall.transform.position.x;
        timerCooldown = 0;
        //PassiveSkillInfluence();

    }

    void PassiveSkillInfluence()
    {
        //pData.defence *= (1+pData.availableattak[3].power);
        pData.hp *= (1 + pData.availableattak[4].power);
        //pData.power *= (1 + pData.availableattak[5].power);
    }

    private void Update()
    {
        AnimatorSet();
        //if (rightMove && this.transform.position.x - wall.transform.position.x >= distanse)
        //{ wall.transform.position = new Vector3(this.transform.position.x - distanse, 0, 0); } //move wall with player

        if (timerCooldown >= 0) { timerCooldown -= Time.deltaTime; }
        if (timerStopMoving >= 0) { timerStopMoving -= Time.deltaTime; }
        //if (timerSwipe >= 0) { timerSwipe -= Time.deltaTime; } else if (triggerColl.radius != previousTriggerCollRadius) { triggerColl.radius = previousTriggerCollRadius; }

        
    }

    public bool RandomSuccess(double probability)  //if (RandomSuccess(0.2)) 20%
    {
        return _rnd.NextDouble() < probability;
    }

    private void AnimatorSet()
    {
        anim.SetInteger("fightId", fightId);
        anim.SetBool("runForward", runForward);
        anim.SetBool("runBackward", runBackward);
        anim.SetBool("fight", fight);
        anim.SetBool("roar", roar);
        anim.SetBool("storm", storm);
        anim.SetBool("swipe", swipe);
    }

    public void GetCrystal()
    {
        FindObjectOfType<AudioManager>().Play("crystal");
        pData.crystals += 1;
    }


    public void GetXp(int xp)
    {
        gm.earnedXp += xp;
        pData.xp += xp;
        dMessage = Instantiate(damageMessage, this.transform.position + 3.5f * transform.up, Quaternion.identity);
        dMessage.transform.GetChild(0).GetComponent<Text>().text = "+" + Convert.ToString(xp);
        if (pData.xp >= pData.maximumXp)
        { LevelUp(); }
    }

    private void LevelUp()
    {
        FindObjectOfType<AudioManager>().Play("levelUp");
        if (pData.xp > pData.maximumXp)
        {
            pData.xp -= pData.maximumXp;
        }
        else { pData.xp = 0;}
        pData.maximumHp += pData.level * 10;
        pData.maximumXp = pData.level * 100 + pData.maximumXp;
        pData.hp /= (1 + pData.availableattak[4].power);
        pData.hp += pData.level * 10;
        pData.level += 1;
        pData.power += 10;
        pData.defence += 5;
        pData.criticalHit += 0.005f;


        Debug.Log(GameObject.Find("LevelUpLight").GetComponent<SpriteRenderer>());
        StartCoroutine(LevelUpBlink(GameObject.Find("LevelUpLight").GetComponent<SpriteRenderer>()));
        PassiveSkillInfluence();
        if (pData.xp > pData.maximumXp)
        { LevelUp(); }

        }

    public void ReceiveDamage(float damagePoints, int enemyLevel)
    {
        if (!getDamage)
        {
            if (gm.levelNum <= 10) { FindObjectOfType<AudioManager>().Play("boar"); }
            else { FindObjectOfType<AudioManager>().Play("wolf"); }
            getDamage = true;
            StartCoroutine("Blink");
            thrustAllow = true;
            
            if (enemyLevel == 100)
            {
                Invoke("Thrust", 0.3f);
                m_Rigidbody2D.AddForce(transform.right * -10, ForceMode2D.Impulse);
            }
            else
            {
                Invoke("Thrust", 0.2f);
                m_Rigidbody2D.AddForce(transform.right * -5, ForceMode2D.Impulse);
            }
            if (damagePoints > pData.defence * (1 + pData.availableattak[3].power))
            { pData.hp -= damagePoints - pData.defence * (1 + pData.availableattak[3].power); }
            if (pData.hp <= 0)
            { Die(); }
        }

    }

    private void Die()
    {
        m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionX;
        m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, 4f);
        this.GetComponent<BoxCollider2D>().isTrigger = true;
        Invoke("PlayerDieFlag",1.6f);
        //Destroy(this.gameObject);
    }

    
    private void PlayerDieFlag()
    {
        this.GetComponent<BoxCollider2D>().isTrigger = false;
        m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezePositionY;
        Time.timeScale = 0;
        isDied = true;
    }

    public void Alive()
    {
        FindObjectOfType<AudioManager>().Play(gm.soundtrackName);
        FindObjectOfType<AudioManager>().Play("appear");
        m_Rigidbody2D.constraints = RigidbodyConstraints2D.None;
        m_Rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
        this.transform.position += new Vector3(0,8f,0);
        pData.hp = pData.maximumHp / 3;
        gm.enemyCountOnLevel -= 1;

        isDied = false;
    }

    public void Move(float move, bool isfight = false, bool isRoar = false, bool isSwipe = false, bool isStorm = false)
    {
        swipe = isSwipe;
        storm = isStorm;
        fight = isfight;
        roar = isRoar;

        if (timerStopMoving > 0)
        {move = 0;}

        if (getDamage)
        {
            timerDamage -= Time.deltaTime;
            if (timerDamage < 0)
            {
                getDamage = false; // allow get damage
                timerDamage = 1.5f; // update getDamage timer
                StopCoroutine("Blink"); // stop blinking
                sprite.color = new Color(sprite.color.r, 255, 255, 1); // return to panther normal color
            }
        }

        if (fight || roar || storm || swipe)
        {timerStopMoving = 0.5f;}

        if (fight)
        {
            if (fightId != 1)// alternation of simple hits
            { fightId += 1; }
            else { fightId = 0; }
        }
        if (roar)
        { Roar(); }
        if (swipe)
        { Swipe(); }
        if (storm)
        { Storm(); }

        if (move > 0)
        {
            runForward = true;
            runBackward = false;
            rightMove = true;
            m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y);
        }
        else if (move < 0)
        {
            rightMove = false;
            runBackward = true;
            runForward = false;
            m_Rigidbody2D.velocity = new Vector2(move * m_MinSpeed, m_Rigidbody2D.velocity.y);
        }
        else if (!thrustAllow)
        {
            runBackward = false;
            runForward = false;
            m_Rigidbody2D.velocity = new Vector2(0, m_Rigidbody2D.velocity.y);
        }
    }

    private void Thrust()
    {
        thrustAllow = false;
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if (triggerColl.isActiveAndEnabled)
        {
            if ((coll.gameObject.tag == "Enemy" || coll.gameObject.tag == "BossEnemy") && fight)
            {
                float attackPower = HitEnemy(pData.power * (1 + pData.availableattak[5].power));
                if ( timerCooldown <= 0)
                {
                    FindObjectOfType<AudioManager>().Play("hit");
                    if (gm.levelNum % 10 == 0)
                    { coll.gameObject.GetComponent<BossMovement>().ReceiveDamage(attackPower); }
                    else { coll.gameObject.GetComponent<EnemyMovement>().ReceiveDamage(attackPower); }
                    timerCooldown = 0.1f;
                }
            }
        }
    }

    float HitEnemy(float power)
    {
        if (RandomSuccess(pData.criticalHit))
        { power *= 2; }
        return power;
    }

    void Roar()
    {
        if(!roarCooldown)// stop all enemies for cooldown seconds
        {
            FindObjectOfType<AudioManager>().Play("roar");
            GameObject[] allEnemies;
            allEnemies =(gm.levelNum % 10 != 0)? GameObject.FindGameObjectsWithTag("Enemy"): GameObject.FindGameObjectsWithTag("BossEnemy"); 
            foreach (GameObject enemy in allEnemies)
            {
                if (gm.levelNum % 10 != 0) { enemy.GetComponent<EnemyMovement>().ListenRoar(pData.availableattak[0].power); }
                else { enemy.GetComponent<BossMovement>().ListenRoar(pData.availableattak[0].power); };
            }
            roarCooldown = true;
            Invoke("UpdateRoarCooldown", pData.availableattak[0].cooldown);
        }
    }

    void Swipe()
    {
        FindObjectOfType<AudioManager>().Play("swipe");
        swipeObj.SetActive(true);
        timerSwipe = 0.3f;
        swipeCooldown = true;
        Invoke("UpdateSwipeCooldown", pData.availableattak[1].cooldown);
    }

    void Storm()
    {
        GameObject[] allEnemies;
        
        if (gm.levelNum % 10 == 0)
        {
            allEnemies = GameObject.FindGameObjectsWithTag("BossEnemy");  }
        else
        {
            allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        }
        foreach (GameObject enemy in allEnemies)
        {
            float attackPower = HitEnemy(pData.power*(1 + pData.availableattak[2].power));
            if (gm.levelNum % 10 == 0)
            {
                enemy.GetComponent<BossMovement>().GetThrust(4f);
                enemy.GetComponent<BossMovement>().ReceiveDamage(attackPower);
            }
            else {enemy.GetComponent<EnemyMovement>().GetThrust(4f);
            enemy.GetComponent<EnemyMovement>().ReceiveDamage(attackPower); }
            
        }
        FindObjectOfType<AudioManager>().Play("storm");
        stormCooldown = true;
        Invoke("UpdateStormCooldown", pData.availableattak[2].cooldown);
    }

    void UpdateRoarCooldown()
    {
        roarCooldown = false;
    }
    void UpdateSwipeCooldown()
    {
        swipeCooldown = false;
    }
    void UpdateStormCooldown()
    {
        stormCooldown = false;
    }

    public void stopInvoke()
    {
        CancelInvoke();
    }
    IEnumerator LevelUpBlink(SpriteRenderer whiteBlurredCircle)
    {
        bool back = false;
        while (true)
        {
            if (!back)
            {
                if (whiteBlurredCircle.color.a >= 2f)
                {

                    back = true;
                }
                else if (whiteBlurredCircle.color.a >= 1f)
                {
                    whiteBlurredCircle.color = new Color(255, 255, 255, whiteBlurredCircle.color.a * 2);
                    yield return new WaitForSeconds(3f);
                }
                else
                {
                    whiteBlurredCircle.color = new Color(255, 255, 255, whiteBlurredCircle.color.a + 0.1f);
                    yield return new WaitForSeconds(0.05f);
                    //break;
                }
            }
            else
            {
                if (whiteBlurredCircle.color.a <= 0)
                {
                    whiteBlurredCircle.color = new Color(255, 255, 255, 0);
                    yield break;
                }
                else
                {
                    whiteBlurredCircle.color = new Color(255, 255, 255, whiteBlurredCircle.color.a - 0.1f);
                    yield return new WaitForSeconds(0.1f);
                }
            }
        }
    }

    IEnumerator Blink()
    { // red blinking
        while (true)
        { 
            switch (sprite.color.g.ToString())
            {
                case "1":
                case "255":
                    sprite.color = new Color(sprite.color.r, 0, 0, 1);
                    //Play sound
                    yield return new WaitForSeconds(0.1f);
                    break;
                default:
                    sprite.color = new Color(sprite.color.r, 255, 255, 1);
                    //Play sound
                    yield return new WaitForSeconds(0.1f);
                    break;
            }
        }
        
    }
}


